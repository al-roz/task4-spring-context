package ru.csu.task4springcontext.service.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import ru.csu.task4springcontext.service.MessageRenderer;

@Log4j2
@Primary
@Component
public class MessageLoggerRender implements MessageRenderer {

    public MessageLoggerRender() {
        System.out.println();
    }

    @Override
    public void render(String message) {
        log.info(message);
    }
}
